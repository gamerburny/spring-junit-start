package at.spenger.junit.domain;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


public class Club {
	private List<Person> l;
	
	public Club() {
		l = new ArrayList<>();
	}
	
	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}
	
	public int numberOf() {
		return l.size();
	}
	
	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}

	public double averageAge() {
	
		if(numberOf() != 0) {
			double d = 0;
			for(Person p : l) {
				d += Period.between(p.getBirthday(), LocalDate.now()).getYears();
			}
			return d / numberOf();
		}
		return 0;
	}
	
	public List<Person> sortByName() {
		if(numberOf() != 0){
			Collections.sort(l);
		}
		return getPersons();
	}
	 
	public List<Person> remove(int index) {
		if(numberOf() != 0 && index < numberOf()) {
			l.remove(index);
			return Collections.unmodifiableList(l);
		}
		return null;
	}
	
	public Person oldestPerson() {
		
		if(numberOf() != 0) {
			Collections.sort(l, new AgeComparator());
			return l.get(0);
		}
		return null;
	}
	
	public Person youngestPerson() {
		if(numberOf() != 0) {
			Collections.sort(l, new AgeComparator());
			Collections.reverse(l); //da der AgeComparator den �ltesten zuerst und den J�ngsten zuletzt in die Liste ordnet wird durch reverse die Reihenfolge umgedreht
			return l.get(0);
		}
		return null;
	}
	
	public void sortName() {
		l = l.stream()
			.sorted( (x, y) -> x.getLastName().compareTo(y.getLastName()))
			.collect(Collectors.toList());
	}
	
	public void sortOldestPerson() {
		l = l.stream()
			.sorted( (x, y) -> x.getBirthdayString().compareTo(y.getBirthdayString()))
			.collect(Collectors.toList());
	}
	
	public void sortYoungestPerson() {
		l = l.stream()
			.sorted( (x, y) -> y.getBirthdayString().compareTo(x.getBirthdayString()))
			.collect(Collectors.toList());
	}

	public double durchschnittsalter() {
		double a = l.stream()
				.collect(Collectors.averagingInt(Person::getAge));
		return a;
	}

	public void austreten(int i) {
		if (l.get(i) == null) {
			throw new IllegalArgumentException("person is null!");
		}
		l.remove(i);
	}
}
