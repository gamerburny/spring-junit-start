package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import at.spenger.junit.domain.Person.Sex;

public class ClubTest {
	
	@Test
	public void testEnter() {
		 Club c = new Club();
		 Person p = new Person("Hans", "Peter", LocalDate.now(), Sex.MALE );
		 boolean erg = c.enter(p);
		 boolean erwartet = true;
		 assertEquals(erwartet, erg);
	}

	@Test
	public void testNumberOf() {
		
		Club c = new Club();
		 
		int erg = c.numberOf();
		int erwartet = 0;
		assertEquals(erwartet, erg);
	}
		
	@Test
	public void testAverageAge() {
		
		Club c = new Club();
		Person p1 = new Person("Hans", "Peter", LocalDate.now().minusYears(20), Sex.MALE );
		Person p2 = new Person("Heinz", "Peter", LocalDate.now().minusYears(10).plusMonths(3).minusDays(10), Sex.MALE );
		
		c.enter(p1);
		c.enter(p2);
				
		double erg = c.averageAge();
		double erwartet = 14.5;
		assertEquals(erwartet, erg, 0.0000000001); 
		// anscheinend werden Doubles obwohl sie den gleichen Wert haben 
		// nicht mit gleichem Wert gespeichert 
		// assertEquals(erwartet, erg); => 14.5 ist nicht 14.5
	
	}
	
	@Test
	public void testSortByName() {
		Club c = new Club();
		Person p1 = new Person("Karl", "Quop", LocalDate.now().minusYears(10).plusMonths(3).minusDays(10), Sex.MALE );
		Person p2 = new Person("Heinz", "Ahorn", LocalDate.now().minusYears(20), Sex.MALE );
		Person p3 = new Person("Hans", "Peter", LocalDate.now().minusYears(20), Sex.MALE );
		
		c.enter(p1);
		c.enter(p2);
		c.enter(p3);
		
		Club check = new Club();
		
		check.enter(p2);
		check.enter(p3);
		check.enter(p1);
		
		List<Person> erg = c.sortByName();
		List<Person> erwartet = check.getPersons();
		
		assertEquals(erwartet, erg);
	}
	
	//Austreten
	@Test
	public void testRemove() {
		Club c = new Club();
		Person p1 = new Person("Karl", "Quop", LocalDate.now().minusYears(10).plusMonths(3).minusDays(10), Sex.MALE );
		Person p2 = new Person("Heinz", "Ahorn", LocalDate.now().minusYears(20), Sex.MALE );
		Person p3 = new Person("Hans", "Peter", LocalDate.now().minusYears(20), Sex.MALE );
		
		c.enter(p1);
		c.enter(p2);
		c.enter(p3);
		
		Club check = new Club();
		
		check.enter(p1);
		check.enter(p3);
		
		List<Person> erg = c.remove(1);
		List<Person> erwartet = check.getPersons();
		
		assertEquals(erwartet, erg);
		
	}
	//2 weitere
	@Test
	public void testOldestPerson() {
		Club c = new Club();
		Person p1 = new Person("Karl", "Quop", LocalDate.now().minusYears(10).plusMonths(3).minusDays(10), Sex.MALE );
		Person p2 = new Person("Heinz", "Ahorn", LocalDate.now().minusYears(20), Sex.MALE );
		Person p3 = new Person("Hans", "Peter", LocalDate.now().minusYears(20).minusDays(5), Sex.MALE );
		
		c.enter(p1);
		c.enter(p2);
		c.enter(p3);

		Person erwartet = p3;
		Person erg = c.oldestPerson();
				
		assertEquals(erwartet, erg);
	} 
	
	@Test
	public void testYoungestPerson() {
		Club c = new Club();
		Person p1 = new Person("Karl", "Quop", LocalDate.now().minusYears(10).plusMonths(3).minusDays(15), Sex.MALE );
		Person p2 = new Person("Heinz", "Ahorn", LocalDate.now().minusYears(10).plusMonths(3).minusDays(10), Sex.MALE );
		Person p3 = new Person("Hans", "Peter", LocalDate.now().minusYears(10).plusMonths(3).minusDays(20), Sex.MALE );

		c.enter(p1);
		c.enter(p2);
		c.enter(p3);

		Person erwartet = p2;
		Person erg = c.youngestPerson();
				
		assertEquals(erwartet, erg);
		
	}
	
	@Test
	public void testSortName() {
		Club c = new Club();
		Person p1 = new Person("Karl", "Quop", LocalDate.now().minusYears(10).plusMonths(3).minusDays(10), Sex.MALE );
		Person p2 = new Person("Heinz", "Ahorn", LocalDate.now().minusYears(20), Sex.MALE );
		Person p3 = new Person("Hans", "Peter", LocalDate.now().minusYears(20), Sex.MALE );
		
		c.enter(p1);
		c.enter(p2);
		c.enter(p3);
		
		c.sortName();
		List<Person> l = c.getPersons();
		assertTrue(l.get(0).getLastName().equals("Ahorn"));
		
	}
	
	@Test
	public void testAlter()
	{
		
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);

		Instant fixedInstant = Instant.parse("2010-01-02T11:00:00Z");
		Clock fixedClock = Clock
	       .fixed(fixedInstant, ZoneId.of("Europe/Vienna"));
	  
		ReflectionTestUtils.setField(p, "clock", fixedClock);
	  
		int alter = p.getAge();
		assertEquals(59, alter);
	}
	
	@Test
	public void testSortOldestPerson() {
		Club c = new Club();
		Person p1 = new Person("Karl", "Quop", LocalDate.now().minusYears(10).plusMonths(3).minusDays(15), Sex.MALE );
		Person p2 = new Person("Heinz", "Ahorn", LocalDate.now().minusYears(10).plusMonths(3).minusDays(10), Sex.MALE );
		Person p3 = new Person("Hans", "Peter", LocalDate.now().minusYears(10).plusMonths(3).minusDays(20), Sex.MALE );

		c.enter(p1);
		c.enter(p2);
		c.enter(p3);

		c.sortOldestPerson();
		List<Person> l = c.getPersons();

		assertTrue(l.get(0).getLastName().equals("Peter"));
	}
	
	@Test
	public void testSortYoungestPerson() {
		Club c = new Club();
		
		Person p1 = new Person("Karl", "Quop", LocalDate.now().minusYears(10).plusMonths(3).minusDays(15), Sex.MALE );
		Person p2 = new Person("Heinz", "Ahorn", LocalDate.now().minusYears(10).plusMonths(3).minusDays(10), Sex.MALE );
		Person p3 = new Person("Hans", "Peter", LocalDate.now().minusYears(10).plusMonths(3).minusDays(20), Sex.MALE );

		c.enter(p1);
		c.enter(p2);
		c.enter(p3);

		c.sortYoungestPerson();
		List<Person> l = c.getPersons();

		assertTrue(l.get(0).getLastName().equals("Ahorn"));
	}
	
	@Test
	public void testDurchschnittsalter() {
		Club c = new Club();
		
		Person p1 = new Person("Karl", "Quop", LocalDate.now().minusYears(10).minusDays(5), Sex.MALE );
		Person p2 = new Person("Heinz", "Ahorn", LocalDate.now().minusYears(15).minusDays(5), Sex.MALE );
		Person p3 = new Person("Hans", "Peter", LocalDate.now().minusYears(20).minusDays(5), Sex.MALE );

		c.enter(p1);
		c.enter(p2);
		c.enter(p3);

		double erg = c.durchschnittsalter();

		assertEquals(15, erg, 0.0000001);
	}
	
	@Test
	public void testGruppierung() {
		Club c = new Club();
		Person p1 = new Person("Karl", "Quop", LocalDate.now().minusYears(10).minusDays(5), Sex.MALE );
		Person p2 = new Person("Heinz", "Ahorn", LocalDate.now().minusYears(14).minusDays(5), Sex.MALE );
		Person p3 = new Person("Hans", "Peter", LocalDate.now().minusYears(14).minusDays(5), Sex.MALE );

		c.enter(p1);
		c.enter(p2);
		c.enter(p3);

		  
		  HashMap<Integer, List<Person>> map = new HashMap<Integer, List<Person>>();
		  List<Person> m8 = new ArrayList<Person>(c.getPersons());
		  m8.sort((Person x, Person y) -> x.getBirthday().getYear() - y.getBirthday().getYear());
		  List<Person> m7 = null;
		  for(Person p: m8)
		  {
		      m7 = m8.stream().filter(x -> x.getBirthday().getYear() == p.getBirthday().getYear()).collect(Collectors.toList());
		   for(Person pers: m7)
		   {
		    map.put(pers.getBirthday().getYear(), m7);
		   }
		  }
		  for(Integer i : map.keySet())
		  {
		   System.out.println("Jahr "+i+ " Persons: "+ Arrays.toString(map.get(i).toArray()) + System.lineSeparator());
		  }
		  assertEquals(2, map.get(2000).size());
	}
	
	@Test 
	public void testAustreten() {
		Club c = new Club();
		
		Person p1 = new Person("Karl", "Quop", LocalDate.now().minusYears(10).minusDays(5), Sex.MALE );
		Person p2 = new Person("Heinz", "Ahorn", LocalDate.now().minusYears(14).minusDays(5), Sex.MALE );
		Person p3 = new Person("Hans", "Peter", LocalDate.now().minusYears(14).minusDays(5), Sex.MALE );

		c.enter(p1);
		c.enter(p2);
		c.enter(p3);		
		
		long count = c.getPersons().stream().filter(p -> p.getLastName().equals("Peter")).count();
		
		c.austreten(2);
			
		long count2 = c.getPersons().stream().filter(p -> p.getLastName().equals("Peter")).count();

		assertNotEquals(count, count2);
	}
	
}
